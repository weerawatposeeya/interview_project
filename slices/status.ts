import { createSlice, PayloadAction } from '@reduxjs/toolkit'

interface StatusState {
  value: string
}

const initialState = { value: 'all' } as StatusState

const counterSlice = createSlice({
  name: 'status',
  initialState,
  reducers: {
    updateNewStatus(state, action) {
      // a new value will replace the old one
      return {
        ...state,
        value: action.payload
      }
    },
  },
})

export const { updateNewStatus } = counterSlice.actions
export default counterSlice.reducer