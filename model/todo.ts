interface Todo {
    map: any
    id: string
    title: string
    completed: boolean
}

export default Todo