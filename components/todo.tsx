import { FunctionComponent, useEffect, useState } from "react"
import styled from 'styled-components'
import { Popup, SaveButton} from '@/components/UIKits'
import { useUpdateTodoMutation, useDeleteTodoMutation, useGetTodoQuery } from '@/service/todo'
import { motion } from "framer-motion"
import Todo from '@/model/todo';

type Props = {
    status: string;
    isChecked: boolean;
    todosID: string,
    index: number
}


const Item = styled.div `
    background: #FFFFFF;
    border-radius: 25px;
    padding: 5px;
    width: 100%;
    min-width:100%;
    filter: drop-shadow(0px 0px 20px rgba(0, 0, 0, 0.03));
    margin-bottom: 25px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    min-height: 46px;
    position: relative;
`

const Loading = styled.div `
  min-height: 46px;
  min-width:100%;
  background: #FFFFFF;
  border-radius: 25px;
  padding: 10px 20px;
  margin-bottom: 25px;
`

const Todo: FunctionComponent<Props> = ({ todosID, status, index}) => {
    const [editing, setEditing] = useState(false)
    const [popup, setPopup] = useState(false)
    const [title, setTitle] = useState("")
    const { data, isFetching } = useGetTodoQuery(todosID)
    const [updateTodo, { isLoading: isUpdating }] = useUpdateTodoMutation()
    const [deleteTodo, { isLoading: isDeleting }] = useDeleteTodoMutation()

    const handleChange = async (val: boolean, status: string) => {
      console.log('change')
      try {
          await updateTodo({completed: val , id: todosID, title: status }).unwrap()
      } catch {
         console.log('error')
      } finally {
        setPopup(false)
        setEditing(false)
      }
    }
     
  useEffect(() => {
    setTitle(status)
  }, [])

  if (!data) return null

  if (isFetching) {
    return <Loading>Loading...</Loading>
  }


  

    
  return (
      <>
        {  !popup ? data.map((item : Todo) => {
          return (
            <motion.div
                animate={{ opacity : 1 }}
                transition={{from: 0}}
                key={item.id}
                className="w-100"
              >
            <Item key={item.id} style={{zIndex: index}}>
              <input className="my-auto" onChange={() => handleChange(!item.completed ,item.title ) }  checked={item.completed} type="checkbox" name="checkbox" />
              <label className={`my-auto  ${item.completed  && 'line-through'}`}>{item.title}</label>
              <div className="pr-10px three-dot " onClick={() => setPopup(true)}>...</div>
            </Item> 
            </motion.div>
            )}) : 
            data.map((item : {title: string, completed: boolean , id: string}) => {
              return ( <Item  key={item.id} style={{zIndex: index}}> 
                {!editing && <input className="my-auto" onChange={() => handleChange(!item.completed ,item.title ) }  checked={item.completed} type="checkbox" name="checkbox" />}
              <input type="text" className={!editing ? `ml-0` : ''} onChange={(e) => setTitle(e.target.value)}  value={title}  />
              <div className="flex-col">
              {editing ? 
                (title !== '') && <SaveButton onClick={() => handleChange(item.completed ,title )} >{isUpdating ? 'Updating...' : 'Save'}</SaveButton> :
                
                  <Popup>
                    <button className="edit-btn" onClick={() => setEditing(true)} >Edit</button>
                    <button className="delete-btn" onClick={() => deleteTodo(todosID)} disabled={isDeleting}>{isDeleting ? 'Deleting...' : 'Delete'}</button>
                  </Popup>}
              </div>
            </Item>
            )})
        }
      </>
  )
  
}



export default Todo

