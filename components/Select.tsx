import styled from 'styled-components'
import { useState } from 'react';


const Wrapper = styled.div`
    position: relative;
    z-index: 10;
`

const Button = styled.button`
    min-width: 110px;
    width: 100%;
    border-radius: 10px;
    padding:  7px 10px;
    border: 0;
    text-align: left;
    background-size: contain;
    background: #FFF url("data:image/svg+xml;utf8,<svg viewBox='0 0 140 140' width='8' height='8' xmlns='http://www.w3.org/2000/svg'><g><path d='m121.3,34.6c-1.6-1.6-4.2-1.6-5.8,0l-51,51.1-51.1-51.1c-1.6-1.6-4.2-1.6-5.8,0-1.6,1.6-1.6,4.2 0,5.8l53.9,53.9c0.8,0.8 1.8,1.2 2.9,1.2 1,0 2.1-0.4 2.9-1.2l53.9-53.9c1.7-1.6 1.7-4.2 0.1-5.8z' fill='black'/></g></svg>") no-repeat ;
    background-position: right 10px top 50%;
`

const WrapOption = styled.div`
    width: 100%;
    display: flex;
    flex-direction: column;
    background: #FFFFFF;
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
    padding: 10px 6px;
    position: absolute;
    left: 0;
    top: 35px;
`

const OptionActive = styled.div`
cursor: pointer;
    min-width: 98px;
    width: 100%;
    background: #585292;
    border-radius: 8px;
    padding: 5px 8px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 16px;
    text-align: left;
    /* identical to box height */
    text-transform: capitalize;
    color: #FFFFFF;
`

const OptionInActive = styled.div`
    cursor: pointer;
    min-width: 98px;
    width: 100%;
    background: #fff;
    border-radius: 8px;
    padding: 5px 8px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 16px;
    text-align: left;
    /* identical to box height */
    text-transform: capitalize;
    color: #000;
`


const Select = (props: {filter: 'ALl' | 'Done' | 'Undone', handlerClickOption: CallableFunction, isShow: boolean , handlerClickIsShow: CallableFunction} ) => {
    const {filter, handlerClickOption, handlerClickIsShow , isShow} = props

    const allFilter = ['All' , 'Done' , 'Undone']
    return(
        <Wrapper>
            <Button onClick={() => handlerClickIsShow(!isShow)}>{filter}</Button>
            {isShow && 
            <WrapOption >
                
                {
                    allFilter.map((item) => {
                        return (
                            filter === item ? 
                            <OptionActive key={item} onClick={() => handlerClickOption(item)}>{item}</OptionActive>:
                            <OptionInActive key={item} onClick={() => handlerClickOption(item)}>{item}</OptionInActive>
                        )
                    })
                }
            </WrapOption>
            }
        </Wrapper>
    )
}

export default Select