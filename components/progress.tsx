
import {  useGetTodosQuery } from '@/service/todo';
import styled from 'styled-components';
import { motion } from "framer-motion"

const Progress = styled.div `
    min-height: 7.34px;
    width: 100%;
    max-width: 100%;
    background-color: #3B3B3B;
    position: relative;
    margin: 12px 0;
    border-radius: 5px;
`
const Wrapper = styled.div`
    min-height: 123px;
    width: 100%;
    max-width: 100%;
    border-radius: 20px;
    padding: 19px 25px;
    background-color: #E07C7C;
    color: #FFF;
`

const Complete = styled.span`
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 16px;
    line-height: 19px;
    color: #EBB9B8;
`

const ProgressBar = () => {
    const { data , error } = useGetTodosQuery()

    let progression = 0
    // finding 100% (amount of data / total * 100%)
    const acheivedLength  = data && data.filter((todo) => todo.completed === true)
    if(data){
        progression = acheivedLength ? ((acheivedLength.length / data.length) * 100) : 0
    }
    
    return (
        <Wrapper>
            <h2>Progress</h2>
            <Progress>
                <motion.div
                    animate={{
                        x: 0,
                        height: `100%`,
                        width: `${progression}%`,
                        backgroundColor: "#ffffff",
                        borderRadius: "5px",
                        position: "absolute",
                        left: 0,
                        top: 0,
                        transitionEnd: {
                            display: "flex",
                        },
                    }}
                />
            </Progress>
            <Complete>{`${ acheivedLength ?  acheivedLength.length : 0} complated`}</Complete>
        </Wrapper>
    )
}


export {ProgressBar};
    