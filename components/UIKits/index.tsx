import styled from 'styled-components';


const Container = styled.div `
    max-width: 518px;
    width: 100%;
    display: flex;
    flex-direction: column;
`

const Item = styled.div `
    background: #FFFFFF;
    border-radius: 25px;
    padding: 10px 20px;
    width: 100%;
    max-width:100%;
    display: flex;
    flex-direction: row;
    align-content: center;
    justify-content: space-between;
    filter: drop-shadow(0px 0px 20px rgba(0, 0, 0, 0.03));
`


const Wrapper = styled.div `
    max-width: 720px;
    width: 100%;
    display: flex;
    flex-direction: column;
    background-color: #F5F5F5;
    padding: 9.67% 13.9%;
    border-radius: 20px;
    @media screen and (min-width: 720px){
        padding: 65px 100px;
    }
`

const SaveButton = styled.div `
    background: #585292;
    border-radius: 999px;
    padding: 10px 17px;
    font-family: 'Roboto';
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    color: #FFF;
    cursor: pointer;
`

const Popup = styled.div`
    padding: 15px 10px;
    background-color: #FFF;
    position: absolute;
    top: -60%;
    right: 12px;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
    border-radius: 10px;
`

export {Container, Item , Wrapper, SaveButton , Popup};
    