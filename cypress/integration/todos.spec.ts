/// <reference types="cypress" />

describe('example to-do app', () => {
    const newItem = 'Feed the cat'
    beforeEach(() => {
    //   visit local frontend rootPath
      cy.visit('http://localhost:3000')
    })
  
    it('displays three todo items by default', () => {
      // which are the three default items.
      cy.get('label').should('have.length', 3)

      // to get just the first and last matched elements individually,
      // and then perform an assertion with `should`.
      cy.get('label').first().should('have.text', 'Buy food for dinner')
      cy.get('label').last().should('have.text', 'Write a react blog post')
    })
  
    it('can add new todo items', () => {
      
      // input our new list item. After typing the content of our item
      cy.get('.defualt-add-todo').type(`${newItem}{enter}`)
      cy.get('.defualt-save-new-todo').click()
  
      cy.get('label')
        .should('have.length', 4)
        .last()
        .should('have.text', newItem)
    })
  
    it('can check off an item as completed', () => {
      // However, this will yield the <label>, which is lowest-level element that contains the text.
      // In order to check the item, we'll find the <input> element for this <label>
      // by traversing up the dom to the parent element. From there, we can `find`
      // the child checkbox <input> element and use the `check` command to check it.
      cy.contains('Feed the cat')
        .parent()
        .find('input[type=checkbox]')
        .check()
  
      // to traverse multiple levels up the dom until we find the corresponding <li> element.
      // Once we get that element, we can assert that it has the completed class.
      cy.contains('Feed the cat')
        .should('have.class', 'line-through')
    })
  
    context('with a checked task', () => {
      beforeEach(() => {
        cy.contains('Call Marie at 10.00 PM')
          .parent()
          .find('input[type=checkbox]')
          .check()
      })
  
      it('can filter for uncompleted tasks', () => {
        // We'll click on the "active" button in order to
        // display only incomplete items
        cy.get('button').contains('All').click()
        cy.get('div').contains('Undone').click()
  
        // After filtering, we can assert that there is only the one
        // incomplete item in the list.
        cy.get('label')
          .should('have.length', 1)
          .first()
          .should('have.text', 'Write a react blog post')
  
        // For good measure, let's also assert that the task we checked off
        // does not exist on the page.
        cy.contains('Call Marie at 10.00 PM').should('not.exist')
      })
  
      it('can filter for completed tasks', () => {
        // that only completed tasks are shown
        cy.get('button').contains('All').click()
        cy.get('div').contains('Done').click()
  
        cy.get('label')
          .should('have.length', 3)
          .first()
          .should('have.text', 'Buy food for dinner')
  
        cy.contains('Write a react blog post').should('not.exist')
      })



      it('can user delete task', () => {
        // that only completed tasks are shown
        // cy.get('button').contains('All').click()
        // cy.get('div').contains('Done').click()

        cy.get('label').contains('Buy food for dinner').parent().find('div').click()
        cy.get('.delete-btn').contains('Delete').click()
  
        cy.get('label')
          .should('have.length', 3)
          .first()
          .should('have.text', 'Call Marie at 10.00 PM')
  
        cy.contains('Buy food for dinner').should('not.exist')
      })


      it('can user edit task ', () => {
        // that only completed tasks are shown
        // cy.get('button').contains('All').click()
        // cy.get('div').contains('Done').click()

        cy.get('label').contains('Call Marie at 10.00 PM').parent().find('div').click()
        cy.get('.edit-btn').contains('Edit').click()

        cy.get('input').first().type(`${' Say something'}{enter}`)

        cy.get('div').contains('Save').first().click()
  
        cy.get('label')
          .should('have.length', 3)
          .first()
          .should('have.text', 'Call Marie at 10.00 PM Say something')
  
      })
      
    })
  })
  