import type { NextPage } from 'next'
import Head from 'next/head'
import Todo from '@/components/todo'
import styles from '../styles/Home.module.css'
import {  useEffect, useState } from 'react'
import { Container, Wrapper, SaveButton} from '@/components/UIKits'
import styled from 'styled-components'
import { useGetTodosQuery, useAddTodoMutation } from '@/service/todo'
import { ProgressBar } from '@/components/progress'
import Select from '../components/Select';


const Item = styled.div `
    background: #FFFFFF;
    border-radius: 25px;
    padding: 5px;
    width: 100%;
    max-width:100%;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    min-height: 46px;
    filter: drop-shadow(0px 0px 20px rgba(0, 0, 0, 0.03));
`

const Home: NextPage = () => {
  const { data , error, isLoading } = useGetTodosQuery('')
  const [addTodo, { isLoading: isAdding }] = useAddTodoMutation()

  const [todo, setTodo] = useState({} as any)
  const [todos, setTodos] = useState(() => data)
  const [status, setStatus] = useState('All' as any)
  const [isShow, setisShow] = useState(false)

  const handleAddTodo = async () => {
    if(todo.title !== '') {
      try {
        await addTodo(todo).unwrap()
      } catch {
        console.log('An error occurred')
      } finally {
        setTodo({title: ''})
      }
    } 
  }

  useEffect(() => {
    if(status === "Done") {
      setTodos(data ? data.filter((todo) => todo.completed === true) : [])
    }else if(status === "Undone") {
      setTodos(data ? data.filter((todo) => todo.completed === false) : [])
    }else {
      setTodos(data)
    }
    
  }, [status, isAdding, data])


  function handleAddTodoClick(e: { target: { value: string } }){
    setTodo({title: e.target.value, completed: false})
  }

  // child  component Selete callback
  function handlerClickOption(status: string){
    setStatus(status)
    setisShow(false)
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Todos App</title>
        <meta name="description" content="Todo demo next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <Wrapper>
          <Container>{isLoading ? '' : <ProgressBar ></ProgressBar>}</Container>
          <div className="flex-row between mx-15">
            <h2>Tasks</h2>
            <Select filter={status} handlerClickOption={handlerClickOption} isShow={isShow} handlerClickIsShow={setisShow}  />
            {/* <select name="status" id="status" className="select" onChange={(e) => 
              setStatus(e.target.value)
              } value={status}>
              <option value="">All</option>
              <option value="true">Done</option>
              <option value="false">Undone</option>
            </select> */}
          </div>
          <Container >
          {
            todos && todos.map((item, index) => <Todo status={item.title} todosID={item.id} isChecked={(item.completed)}  key={item.id} index={todos.length - index} />) 
          }
            <Item >
              <input className='defualt-add-todo' placeholder='Add your todo...' type="text" value={todo.title}  onChange={handleAddTodoClick} />
              {(todo.title !== '') && <SaveButton className='defualt-save-new-todo' onClick={handleAddTodo}>Save</SaveButton> }
            </Item>
          </Container>
        </Wrapper>
      </main>
    </div>
  )
}

export default Home
