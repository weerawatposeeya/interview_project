import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'
import Todo from '@/model/todo';

type TodosResponse = Todo[]

export const api = createApi({
  baseQuery: fetchBaseQuery({ baseUrl: 'http://localhost:3001' }),
  tagTypes: ['Todos'],
  endpoints: (build) => ({
    getTodos: build.query<TodosResponse, string>({
      query: () => `todos` ,
      providesTags: (result) =>
        result ? result.map(({ id }) => ({ type: 'Todos', id })) : [],
    }),
    addTodo: build.mutation<Todo, Partial<Todo>>({
      query: (body) => ({
        url: 'http://localhost:3001' + `/todos`,
        method: 'POST',
        body,
      }),
      invalidatesTags: ['Todos'],
    }),
    updateTodo: build.mutation<Todo, Partial<Todo>>({
        query(data) {
          const { id, ...body } = data
          return {
            url: 'http://localhost:3001' +  `/todos/${id}`,
            method: 'PATCH',
            body: data,
          }
        },
        // Invalidates all queries that subscribe to this Todo `id` only.
        // In this case, `getTodo` will be re-run. `getTodos` *might*  rerun, if this id was under its results.
        invalidatesTags: (result, error, { id }) => [{ type: 'Todos', id }],
      }),
    deleteTodo: build.mutation<{ success: boolean; id: number }, string>({
        query(id) {
          return {
            url: 'http://localhost:3001' + `/todos/${id}`,
            method: 'DELETE',
          }
        },
        // Invalidates all queries that subscribe to this Todo `id` only.
        invalidatesTags: (result, error, id) => [{ type: 'Todos', id }],
      }),
    getTodo: build.query<Todo, string>({
        query: (id) => `todos?id=${id}`,
        providesTags: (result, error, id) => [{ type: 'Todos', id }],
    }),
  }),
})

// Auto-generated hooks
export const { useGetTodosQuery, useAddTodoMutation, useUpdateTodoMutation,
    useDeleteTodoMutation, useGetTodoQuery } = api

// Possible exports
export const { endpoints, reducerPath, reducer, middleware } = api
// reducerPath, reducer, middleware are only used in store configuration
// endpoints will have:
// endpoints.getTodos.initiate(), endpoints.getTodos.select(), endpoints.getTodos.useQuery()
// endpoints.addTodo.initiate(), endpoints.addTodo.select(), endpoints.addTodo.useMutation()
// see `createApi` overview for _all exports_